//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int main()
{
    int n, num, sum=0;
    printf("Enter number of integers to be added:");
    scanf("%d",&n);
    for(int i=0;i<n;i++)
    {
        printf("Enter a number:");
        scanf("%d",&num);
        sum+=num;
    }
    printf("The sum of all given numbers is %d",sum);
    return 0;
}

