//Write a program to add two user input numbers using 4 functions.
#include<stdio.h>

float input()
{
    float num;
    printf("Enter number:");
    scanf("%f",&num);
    return num;
}
float add(float num1, float num2)
{
    return num1+num2;
}
void display(float number1, float number2, float addition)
{
    printf("Sum of %f and %f is %f",number1, number2, addition);
}
int main()
{
    float a, b, c;
    a=input();
    b=input();
    c=add(a,b);
    display(a,b,c);
    return 0;
}
