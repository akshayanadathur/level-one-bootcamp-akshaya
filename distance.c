//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

void input (float *x1, float *y1, float *x2, float *y2)
{
	printf("Enter x1 value:");
	scanf("%f",x1);
	printf("Enter y1 value:");
	scanf("%f",y1);
	printf("Enter x2 value:");
	scanf("%f",x2);
	printf("Enter y2 value:");
	scanf("%f",y2);
}

float calculate( float a1, float a2, float b1, float b2)
{
	float dist=sqrt(pow(a2-a1,2) + pow(b2-b1,2));
	return dist;
}

void display(float c1, float d1, float c2, float d2, float dis)
{
	printf("Distance between the 2 points %f,%f and %f,%f is %f",c1,d1,c2,d2,dis);
}

int main()
{
	float x1,y1,x2,y2;
	float distance;
	input(&x1,&y1,&x2,&y2);
	distance=calculate(x1,x2,y1,y2);
	display(x1,y1,x2,y2,distance);
	return 0;
}