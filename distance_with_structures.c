//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct point
{
	float x;
	float y;
}; 

typedef struct point P;

P input()
{
	P p;
	printf("Enter the x and y coordinates for a point:\n");
	scanf("%f%f",&p.x,&p.y);
	return p;
}

float calculate (P point1, P point2)
{
	float distance = sqrt((point2.x - point1.x) * (point2.x - point1.x) + (point2.y - point1.y) * (point2.y - point1.y));
	return distance;
}

void display(P p1,P p2,float dis)
{
	printf("The distance between (%f,%f) and (%f,%f) is %f",p1.x,p1.y,p2.x,p2.y,dis);
}

int main()
{
	float dist;
	P pt1, pt2;
	pt1=input();
	pt2=input();
	dist=calculate(pt1,pt2);
	display(pt1,pt2,dist);
	return 0;
}
