//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction
{
  int n;
  int d;
};

typedef struct fraction F;

F lowest_form (struct fraction f3)
{
  //finding the gcd that the fraction sum should be divided by
  int a, b, c, gcd;
  a = f3.n;
  b = f3.d;
  c = a % b;

  while (c > 0)
    {
      a = b;
      b = c;
      c = a % b;
    }

  gcd = b;

  //dividing the sum by gcd in order to get its lowest form.
  f3.n = f3.n / gcd;
  f3.d = f3.d / gcd;
  return f3;
}

F sum (struct fraction f1, struct fraction f2)
{
  struct fraction f3;
  f3.n = (f1.n * f2.d) + (f1.d * f2.n);
  f3.d = (f1.d * f2.d);
  return f3;
}

void input (struct fraction *f1, struct fraction *f2)
{
  printf ("Enter numerator and denominator for first fraction:");
  scanf ("%d%d", &f1->n, &f1->d);
  printf ("Enter numerator and denominator for second fraction:");
  scanf ("%d%d", &f2->n, &f2->d);
}

void output (struct fraction f1, struct fraction f2, struct fraction f3)
{
  printf ("The sum of %d/%d and %d/%d is %d/%d \n", f1.n, f1.d, f2.n, f2.d, f3.n, f3.d);
}

int main ()
{
  struct fraction f1, f2, f3;
  input (&f1, &f2);
  f3 = sum (f1, f2);
  f3 = lowest_form(f3);
  output (f1, f2, f3);
  return 0;
}
