//WAP to find the sum of n fractions.
#include <stdio.h>

struct fraction
{
    int n, d;
};

void add(int i, struct fraction *sum, struct fraction f)
{
    if(i==0)
	{
	    sum->n = f.n;
        sum->d = f.d;
	}
	else
	{
	    sum->n = (f.n * sum->d)+(f.d * sum->n);
	    sum->d = f.d * sum->d;

	}
}

void lowest_form(struct fraction *sum, int gcd)
{
    sum->n = sum->n/gcd;
    sum->d = sum->d/gcd;
}

void gcd(struct fraction *sum)
{
    int a, b, c, gcd;
    a = sum->n;
    b = sum->d;
    c = a % b;
    while (c > 0)
    {
      a = b;
      b = c;
      c = a % b;
    }
    gcd = b;
    lowest_form(sum, gcd);
}

void output(struct fraction sum)
{
    printf("\nThe sum of the given fractions is %d / %d",sum.n,sum.d);
}

void inputs(int i, struct fraction *f)
{
    printf("\nEnter the numerator and denominator :");
    scanf("%d %d", &f->n, &f->d);
}

void inputnum(int *n)
{
    printf("\nHow many fractions to be added? :");
    scanf("%d", n);
}

struct fraction calc(int n)
{
    struct fraction sum, f[50];
    for(int i=0;i<n;i++)
    {
        inputs(i, &f[i]);
        add(i, &sum, f[i]);
    }
    
    gcd(&sum);
    return sum;   
}

int main()
{
    int n;
    inputnum(&n);
    struct fraction sum = calc(n);
    output(sum);
    return 0;
}