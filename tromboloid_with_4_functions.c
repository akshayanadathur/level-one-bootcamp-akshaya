//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input(float *h, float *d, float *b)
{
    printf("Enter height:");
    scanf("%f",&(*h));
    printf("Enter breadth:");
    scanf("%f",&(*b));
    printf("Enter depth:");
    scanf("%f",&(*d));
}

float calculate(float height, float depth, float breadth)
{
    float x=1/3.0;
    float volume = ((x*((height*depth)+depth))/breadth);
    return volume;
}

void display(float vol)
{
    printf("Volume of tromboloid is %f",vol);
}

int main()
{
    float h, d, b, v;
    input(&h, &d, &b);
    v=calculate(h,d,b);
    display(v);
    return 0;
} 

